# Solutions #

Q1. Write a function that takes three sides of a triangle and answers if it's equilateral, isosceles, or scalene.

Q2. For a single-linked (forward only) list write a function that returns 5th element from the end of the list.
The list can only be walked once (length or size of this list cannot be used).

Q3. Given two lists, write a function that answers if all elements of one list are in the other.

# Execute ??? #

Run 'activator test' from the command line.