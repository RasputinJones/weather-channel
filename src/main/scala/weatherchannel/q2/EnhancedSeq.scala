package weatherchannel.q2

import scala.language.implicitConversions

/**
  * Created by uenyioha on 2/19/16.
  */


class EnhancedSeq[A](seq: Seq[A]) {

  private def nFromLast[A](l:Seq[A], n: Int): Option[A] = {
    if (l.isEmpty) None
    else if (n == 1) l match {
      case h :: Nil => Some(h)
      case h :: t => t.lastOption
    }
    else nFromLast(l.dropRight(1), n - 1)
  }

  def fifthFromLast = nFromLast(seq, 5)

}

object EnhancedSeq {

  implicit def seqToEnhancedSeq[A](seq : Seq[A]) : EnhancedSeq[A] = new EnhancedSeq[A](seq)

}