import weatherchannel.q2.EnhancedSeq._

Seq(0, 0, 0).fifthFromLast

Seq(0, 0, 0, 0, 0, 0).fifthFromLast

Seq(0, 0).fifthFromLast

List(0, 1, 0, 0, 0, 0).fifthFromLast
List(1, 0, 0, 0, 0).fifthFromLast
List(0, 0, 1, 0, 0, 0, 0).fifthFromLast

val list1 = List(1, 2, 3, 2, 1)
val list2 = List(3, 1)
val inter1 = list1.intersect(list2)
val inter2 = list2.intersect(list1)

println(inter1)
println(inter2)
def listAContainsB[T](listA: List[T], listB: List[T]) =
  if (listA.isEmpty || listB.isEmpty) false
  else listA.intersect(listB).length == listB.length


listAContainsB(List(4, 5, 6, 8, 7, 8), List(8, 8, 5, 7))

List(4, 5, 6, 8, 7).intersect(List(8, 5, 8, 7))

List(1, 2, 3, 4, 5).filter(List(1, 2).contains(_))

List(1, 2, 3, 4, 5).zip(List(2, 4, 5, 6, 7))

val s = Seq("apple", "oranges", "apple", "banana", "apple", "oranges", "oranges")
s.groupBy(l => l).map(t => (t._1, t._2.length))
s.groupBy(identity).mapValues(_.size)


def listAContainsB2[T](listA: List[T], listB: List[T]) =
  if (listA.isEmpty && listB.isEmpty) true
  else if (listA.isEmpty || listB.isEmpty) true
  else {
    val listAmap = listA.groupBy(identity).mapValues(_.length)
    val result = listB.foldLeft((listAmap, List.empty[T])){
      case ((map, accum), listBelem) =>
        val count = map.getOrElse(listBelem, 0)
        if (count > 0) (map + (listBelem -> (count - 1)), listBelem :: accum) else (map, accum)
    }

    result._2.length == listB.length
  }

listAContainsB2(List(1), List(0))