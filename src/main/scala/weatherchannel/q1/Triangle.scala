package weatherchannel.q1

import scalaz._
import Scalaz._

/**
  * Created by uenyioha on 2/19/16.
  */

sealed trait TriangleType
case object Equilateral extends TriangleType
case object Isosceles extends TriangleType
case object Scalene extends TriangleType

sealed trait ErrorType
case object ArgError extends ErrorType
case object TriangleCheckError extends ErrorType

object Triangle {

  private def checkArgs(side1: Int, side2: Int, side3: Int) : Validation[ErrorType, (Int, Int, Int)] =
    if ((side1 < 0) || (side2 < 0) || (side3 < 0))
      ArgError.failure
    else
      (side1, side2, side3).success

  private def checkTriangleProperty(side1: Int, side2: Int, side3: Int) : Validation[ErrorType, (Int, Int, Int)] = {

    val allTriangular: Boolean = List(side1, side2, side3)
                                  .permutations
                                  .map(_.splitAt(2))
                                  .forall {
                                    case (a, b) => a.sum > b.sum
                                  }

    if (allTriangular)
      (side1, side2, side3).success
    else
      TriangleCheckError.failure
  }


  def isTriangle(side1: Int, side2: Int, side3: Int): Validation[ErrorType, TriangleType] = {

    import scalaz.Validation.FlatMap._

    for {
      cleanArgs <- checkArgs(side1, side2, side3)
      cleanTriangle <- (checkTriangleProperty _).tupled(cleanArgs)
    } yield {
      val (s1, s2, s3) = cleanTriangle
      val candidateTriangle = List(s1, s2, s3)
      candidateTriangle.distinct.size match {
        case 1 => Equilateral
        case 2 => Isosceles
        case _ => Scalene
      }
    }

  }

}


