package weatherchannel.q3

/**
  * Created by uenyioha on 2/19/16.
  */
object Contains {

  def listAContainsB[T](listA: List[T], listB: List[T]) =
    if (listA.isEmpty && listB.isEmpty) true
    else if (listA.isEmpty || listB.isEmpty) false
    else listA.intersect(listB).length == listB.length


  def listAContainsB2[T](listA: List[T], listB: List[T]) =
    if (listA.isEmpty && listB.isEmpty) true
    else if (listA.isEmpty || listB.isEmpty) false
    else {
      val listAmap = listA.groupBy(identity).mapValues(_.length)
      val result = listB.foldLeft((listAmap, List.empty[T])){
        case ((map, accum), listBelem) =>
          val count: Int = map.getOrElse(listBelem, 0)
          if (count > 0) (map + (listBelem -> (count - 1)), listBelem :: accum) else (map, accum)
      }

      result._2.length == listB.length
    }

}
