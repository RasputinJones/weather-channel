package weatherchannel.q3

import org.scalacheck.Gen
import org.scalatest._
import org.scalatest.prop.GeneratorDrivenPropertyChecks

import scala.util.Random

/**
  * Created by uenyioha on 2/20/16.
  */


class ContainsSpec extends PropSpec with GeneratorDrivenPropertyChecks with ShouldMatchers {

  property("check valid sequence") {
    forAll { (l1: List[Int], l2: List[Int]) =>
      whenever(l1.nonEmpty && l2.nonEmpty) {
        Contains.listAContainsB(Random.shuffle(l1 ++ l2), l1) shouldBe true
        Contains.listAContainsB2(Random.shuffle(l1 ++ l2), l1) shouldBe true
      }
    }

    forAll { (l1: List[String], l2: List[String]) =>
      whenever(l1.nonEmpty && l2.nonEmpty) {
        Contains.listAContainsB(Random.shuffle(l1 ++ l2), l1) shouldBe true
        Contains.listAContainsB2(Random.shuffle(l1 ++ l2), l1) shouldBe true
      }
    }
  }

  property("check invalid sequence") {
    forAll { (l1: List[Int], l2: List[Int]) =>
      whenever(l1.nonEmpty && l2.nonEmpty) {
        Contains.listAContainsB(Random.shuffle(l1.filter(!l2.contains(_))), l2) shouldBe false
        Contains.listAContainsB2(Random.shuffle(l1.filter(!l2.contains(_))), l2) shouldBe false
      }
    }

    forAll { (l1: List[String], l2: List[String]) =>
      whenever(l1.nonEmpty && l2.nonEmpty) {
        Contains.listAContainsB(Random.shuffle(l1.filter(!l2.contains(_))), l2) shouldBe false
        Contains.listAContainsB2(Random.shuffle(l1.filter(!l2.contains(_))), l2) shouldBe false
      }
    }
  }
}
