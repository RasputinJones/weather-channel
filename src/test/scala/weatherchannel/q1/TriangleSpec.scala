package weatherchannel.q1

import org.scalatest._
import org.scalatest.prop.Configuration.PropertyCheckConfig
import prop._

import scalaz.Success

/**
  * Created by uenyioha on 2/20/16.
  */

object AllGen {

  import org.scalacheck.Gen

  val side1Gen = Gen.choose(1, 1000000)
  val side2Gen = Gen.choose(1, 1000000)
  val side3Gen = Gen.choose(1, 1000000)

  val invalidSide1 = Gen.choose(-1000000, 0)
  val invalidSide2 = Gen.choose(-1000000, 0)
  val invalidSide3 = Gen.choose(-1000000, 0)
}

class TriangleSpec extends PropSpec with GeneratorDrivenPropertyChecks with ShouldMatchers {

  import AllGen._

  property("reject invalid arguments") {
    forAll(invalidSide1, invalidSide2, invalidSide3) {
      (a : Int, b: Int, c: Int) =>
        Triangle.isTriangle(a, b, c).disjunction.isLeft shouldBe true
        Triangle.isTriangle(a, b, c).disjunction.isRight shouldBe false
    }

  }

  property("reject invalid triangles") {
    forAll(side1Gen, side2Gen, side3Gen) {
      (a : Int, b: Int, c : Int) => {
          if ((a + b > c) && (a + c > b) && (b + c > a)) {
            Triangle.isTriangle(a, b, c) should matchPattern { case scalaz.Success(_) => }
          } else {
            Triangle.isTriangle(a, b, c) should matchPattern { case scalaz.Failure(TriangleCheckError) =>  }
          }
      }
    }
  }

  property("allow valid triangles") {
    forAll(side1Gen, side2Gen, side3Gen) {
      (a : Int, b: Int, c : Int) => {
        whenever ((a + b > c) && (a + c > b) && (b + c > a)) {
          Triangle.isTriangle(a, b, c) should matchPattern { case scalaz.Success(_) => }
        }
      }
    }
  }

  property("identify isosceles triangles correctly") {
    forAll(side1Gen, side2Gen) {
      (a, b) => {
        whenever(a > b) { // force generation of isosceles triangles
        val isoscelesCheck1 =
          Triangle.isTriangle(a + b, a, a) match {
            case Success(Isosceles) =>
              true
            case _ =>
              false
          }

          val isoscelesCheck2 =
            Triangle.isTriangle(a, a + b, a) match {
              case Success(Isosceles) =>
                true
              case _ =>
                false
            }

          val isoscelesCheck3 =
            Triangle.isTriangle(a, a, a + b) match {
              case Success(Isosceles) =>
                true
              case _ =>
                false
            }

          isoscelesCheck1 && isoscelesCheck2 && isoscelesCheck3 shouldBe true
        }
      }
    }
  }

  property("identify equilateral triangles correctly") {
    forAll(side1Gen) {
      a =>
        Triangle.isTriangle(a, a, a) should matchPattern { case scalaz.Success(Equilateral) => }
      }
    }

  property("identify scalene triangles correctly") {
    forAll(side1Gen, side2Gen, side3Gen) {
      (a : Int, b: Int, c : Int) => {
        whenever ((a + b > c) && (a + c > b) && (b + c > a) && (a != b) && (a != c) && (b != c) )   {
          Triangle.isTriangle(a, b, c) should matchPattern { case scalaz.Success(Scalene) => }
        }
      }
    }
  }
}
