package weatherchannel.q2

import org.scalatest._
import org.scalatest.prop._

/**
  * Created by uenyioha on 2/20/16.
  */

class LinkedListSpec extends PropSpec with GeneratorDrivenPropertyChecks with ShouldMatchers {

  property("always retrieve the 5th from last or none") {

    import EnhancedSeq._

    forAll { l: List[Int] =>

      val response = l.fifthFromLast

      if (l.length >= 5)
        response shouldBe Some(l(l.length - 5))
      else
        response shouldBe None
    }

    forAll { l: List[String] =>

      val response = l.fifthFromLast

      if (l.length >= 5)
        response shouldBe Some(l(l.length - 5))
      else
        response shouldBe None
    }
  }
}
